from flask import Flask
from models import db
from views import configure_views

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///blog.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db.init_app(app)

with app.app_context():
    db.create_all()

configure_views(app)


if __name__ == "__main__":
    app.run(debug=True)
