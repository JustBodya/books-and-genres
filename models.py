from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

book_n_genre = db.Table('book_n_genre',
                        db.Column('book_id', db.Integer,
                                  db.ForeignKey('book.id')),
                        db.Column('genre_id', db.Integer,
                                  db.ForeignKey('genre.id')))


class Book(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=True)
    author = db.Column(db.String(50), nullable=True)
    is_read = db.Column(db.Boolean, default=False)

    genre_id = db.Column(db.Integer, db.ForeignKey('genre.id'))
    genre = db.relationship('Genre',
                            backref=db.backref('genre_books', lazy=True))

    def __init__(self, name, author, is_read=False):
        self.name = name.strip()
        self.author = author.strip()
        self.is_read = is_read

    def __repr__(self):
        return f"<book {self.id}>"


class Genre(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    genre_name = db.Column(db.String(30), nullable=True)

    books = db.relationship('Book', secondary=book_n_genre,
                            back_populates='genre')

    def __init__(self, genre_name):
        self.genre_name = genre_name.strip()

    def __repr__(self):
        return f"<Genre {self.genre_name}>"
