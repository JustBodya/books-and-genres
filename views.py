from flask import render_template, request, redirect, url_for
from models import Book, Genre, db


def configure_views(app):
    @app.route('/')
    def index():
        last_books = Book.query.order_by(Book.id.desc()).limit(15).all()[::-1]
        return render_template('index.html', books=last_books)

    @app.route('/add_book', methods=['POST'])
    def add_book():
        name = request.form['name']
        author = request.form['author']
        genre_name = request.form['genre']
        is_read = request.form.get('is_read') == '1'
        book = Book(name=name, author=author, is_read=is_read)
        genre = Genre.query.filter_by(genre_name=genre_name).first()
        if not genre:
            genre = Genre(genre_name=genre_name)
        book.genre = genre

        db.session.add(book)
        db.session.commit()
        return redirect('/')

    @app.route('/genres/<int:genre_id>')
    def view_genre(genre_id):
        genre = Genre.query.get_or_404(genre_id)
        return render_template('genre.html', genre=genre)

    @app.route('/update_status/<int:book_id>', methods=['POST'])
    def update_status(book_id):
        book = Book.query.get_or_404(book_id)
        book.is_read = 'is_read' in request.form

        db.session.commit()
        return redirect(url_for('view_book', book_id=book.id))
